#pragma once

#include <string>

using namespace std;

class Time {
public:
    static void convertTime(const string& time, int &hours, int &minutes);

    static void addTime(int minutes_to_add, int &hours, int &minutes);

    static int getMinutes(int hours, int minutes);

    static bool compareTime(int h, int m, int _h, int _m);

    static string getString(int hours, int minutes);
};