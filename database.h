#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <functional>

#ifndef RGZ_DB_H
#define RGZ_DB_H

using namespace std;

class DbFile {
public:
    /** Путь к файлу */
    string pathToFile;

    /** Заголовок */
    vector<string> head() const;

    /** Получить элемент из списка */
    map<string, string> operator[](const size_t& index) const;

    /** Размер списка */
    size_t size() const;

    /** Инициализация объекта */
    explicit DbFile(const string& pathToFile, const vector<string>& defaultHead = vector<string>(), const char& delimiter = ',');

    /** Удалить строку */
    void pop(const size_t& index);

    /** Поиск по значению */
    int getLineByValue(const string& column, const string& value) const;

    /** Добавить в список */
    void push(const map<string, string>& line);

    /** Изменение записи в файле. Для добавления новой записи нужно использовать "push" */
    void writeLine(const size_t& index, map<string,string> value);

    /** Поиск по списку */
    vector<size_t> find(const function<bool(map<string, string>)> &query) const;

    /** Поиск по списку. Возвращает самую первую линию, которая подходит к условиям */
    int findOne(const function<bool(map<string, string>)> &query) const;

    int getLast() const;

private:
    /** Заголовок */
    vector<string> _head;

    /** Разделитель */
    char delimiter;

    /** Количество записей в файле */
    size_t _size;

    /** Открыть файл (или сначала создать, если его не существует) */
    static ifstream openFile(const string& path);

    /** Создать временный файл */
    static void createTempFile(const string& path, const string& tempName);
};

#endif //RGZ_DB_H
