#pragma once

#include <vector>
#include <string>

using namespace std;

class Headers{
public:
    static vector<string> getPatients();
    static vector<string> getWorkers();
    static vector<string> getVisits();
    static vector<string> getShedule();
    static vector<string> getPass();
};
