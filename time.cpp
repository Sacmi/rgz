#include "time.h"

void Time::convertTime(const string& time, int &hours, int &minutes)
{
    hours = stoi(time.substr(0, 2));
    minutes = stoi(time.substr(3, 2));
}

void Time::addTime(int minutes_to_add, int &hours, int &minutes)
{
    minutes += minutes_to_add;

    while (minutes >= 60)
    {
        hours += 1;
        minutes -= 60;
    }
}

int Time::getMinutes(int hours, int minutes)
{
    return hours * 60 + minutes;
}

/** больше или равно */
bool Time::compareTime(int h, int m, int _h, int _m)
{
    if (h >= _h)
    {
        if (h > _h)
            return true;

        if (m >= _m)
            return true;
    }

    return false;
}

string Time::getString(int hours, int minutes) {
    string str;

    if (hours / 10 == 0)
        str = "0" + to_string(hours);
    else
        str = to_string(hours);

    str += ":";

    if (minutes / 10 == 0)
        str += "0" + to_string(minutes);
    else
        str += to_string(minutes);

    return str;
}
