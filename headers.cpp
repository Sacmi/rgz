//
// Created by Игорь on 02.06.2020.
//

#include "headers.h"

vector<string> Headers::getPatients() {
    return vector<string>{"name","sex","snils","oms"};
}

vector<string> Headers::getWorkers() {
    return vector<string>{"id", "name", "isdoctor", "specialist", "cabinet", "dismissed", "pass"};
}

vector<string> Headers::getVisits() {
    return vector<string>{"visitorid", "doctorid", "day", "time", "week", "symptoms", "comment"};
}

vector<string> Headers::getShedule() {
    return vector<string>{"id","accept","1","2","3","4","5","6"};
}

vector<string> Headers::getPass() {
    return vector<string>{"id","reason", "to"};
}
